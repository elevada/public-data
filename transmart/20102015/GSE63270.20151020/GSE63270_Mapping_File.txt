filename	category_cd	col_nbr	data_label
GSE63270.txt	Study_ID	1	Study_ID	OMIT
GSE63270.txt		2	Subj_ID
GSE63270.txt	Sample_ID	3	OMIT
GSE63270.txt	Samples_and_Timepoints	4	Site_Id
GSE63270.txt	Samples_and_Timepoints	5	Tissue_Type
GSE63270.txt	Subjects+Demographics	6	Sample Species
GSE63270.txt	Subjects+Demographics	7	Age at Diagnosis
GSE63270.txt	Subjects+Demographics	8	Age at Sample Time
GSE63270.txt	Subjects+Demographics	9	Gender
GSE63270.txt	Subjects+Demographics	10	Race
GSE63270.txt	Samples_and_Timepoints	11	Sample Source
GSE63270.txt	Samples_and_Timepoints	12	Sample Cell Type
GSE63270.txt	Subjects+Medical_History	13	Patient Disease State Broad
GSE63270.txt	Subjects+Medical_History	14	Patient Clinical Status
GSE63270.txt	Subjects+Medical_History	15	Patient AMS/MDL Subtype
GSE63270.txt	Subjects+Medical_History	16	Patient Karyotype Summary
GSE63270.txt	Subjects+Medical_History+Cancer_Characteristics	17	Patient Blast Count
GSE63270.txt	Subjects+Medical_History+Cancer_Characteristics	18	Patient Monocyte Count