filename	category_cd	col_nbr	data_label
GSE34577.txt	Study_ID	1	Study_ID	OMIT
GSE34577.txt		2	Subj_ID
GSE34577.txt	Sample_ID	3	OMIT
GSE34577.txt	Samples_and_Timepoints	4	Site_Id
GSE34577.txt	Samples_and_Timepoints	5	Tissue_Type
GSE34577.txt	Subjects+Demographics	6	Sample Species
GSE34577.txt	Subjects+Demographics	7	Age at Diagnosis
GSE34577.txt	Subjects+Demographics	8	Age at Sample Time
GSE34577.txt	Subjects+Demographics	9	Gender
GSE34577.txt	Subjects+Demographics	10	Race
GSE34577.txt	Samples_and_Timepoints	11	Sample Source
GSE34577.txt	Samples_and_Timepoints	12	Sample Cell Type
GSE34577.txt	Subjects+Medical_History	13	Patient Disease State Broad
GSE34577.txt	Subjects+Medical_History	14	Patient Clinical Status
GSE34577.txt	Subjects+Medical_History	15	Patient AMS/MDL Subtype
GSE34577.txt	Subjects+Medical_History	16	Patient Karyotype Summary
GSE34577.txt	Subjects+Medical_History+Cancer_Characteristics	17	Patient Blast Count
GSE34577.txt	Subjects+Medical_History+Cancer_Characteristics	18	Patient Monocyte Count